#include <iostream>
#include <utility>
#include <array>
#include <algorithm>

#define MAX_SPECIALITIES 10
#define MAX_STUDENTS 450

using std::string;
using std::move;
using std::copy;

class Person {
private:
    string fullname;
    int age;
};

class Speciality {
private:
    int faculty;
    int number;
    string title;
};

class Student: public Person {
private:
    Speciality speciality;
    int entrance_year;
};

class Faculty {
private:
    Person dean;
    Person deputy_dean;
    Speciality specialities[MAX_SPECIALITIES];
    Student students[MAX_STUDENTS];
};

int main() {
    std::cout << "Hello, World!" << std::endl;
    return 0;
}
